﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TheBlueHoseVideoPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            VideoControl.Volume = 100;
            VideoControl.Width = 1080;
            VideoControl.Height = 1920;
        }


        void BrowseClick(Object sender, EventArgs e)
        {
            OpenFileDialog openDlg = new OpenFileDialog();
            openDlg.ShowDialog();
            MediaPathTextBox.Text = openDlg.FileName;
        }

        void PlayClick(object sender, EventArgs e)
        {
            if (MediaPathTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Enter a valid media file");
                return;
            }
            VideoControl.Source = new Uri(MediaPathTextBox.Text);
            VideoControl.Play();
        }
        void PauseClick(object sender, EventArgs e)
        {
            VideoControl.Pause();
        }
        void StopClick(object sender, EventArgs e)
        {
            VideoControl.Stop();
        }

    }
}
